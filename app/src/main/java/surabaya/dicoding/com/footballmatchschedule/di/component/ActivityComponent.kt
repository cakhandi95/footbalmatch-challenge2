package surabaya.dicoding.com.footballmatchschedule.di.component

import dagger.Component
import surabaya.dicoding.com.footballmatchschedule.di.module.ActivityModule
import surabaya.dicoding.com.footballmatchschedule.ui.DetailActivity
import surabaya.dicoding.com.footballmatchschedule.ui.activity.AboutMeActivity
import surabaya.dicoding.com.footballmatchschedule.ui.activity.MainActivity

/**
 * Created by handy on 22/09/18.
 * akang.handy95@gmail.com
 */

@Component(modules = arrayOf(ActivityModule::class))
interface ActivityComponent {

    fun inject(mainActivity: MainActivity)

    fun inject(aboutMeActivity: AboutMeActivity)

    fun inject(detailActivity: DetailActivity)

}