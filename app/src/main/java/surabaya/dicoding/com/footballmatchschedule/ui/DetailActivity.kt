package surabaya.dicoding.com.footballmatchschedule.ui

import android.database.sqlite.SQLiteConstraintException
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_detail.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select
import org.jetbrains.anko.toast
import surabaya.dicoding.com.footballmatchschedule.R
import surabaya.dicoding.com.footballmatchschedule.R.drawable.ic_add_to_favorites
import surabaya.dicoding.com.footballmatchschedule.R.drawable.ic_added_to_favorites
import surabaya.dicoding.com.footballmatchschedule.R.id.add_to_favorite
import surabaya.dicoding.com.footballmatchschedule.di.component.DaggerActivityComponent
import surabaya.dicoding.com.footballmatchschedule.di.module.ActivityModule
import surabaya.dicoding.com.footballmatchschedule.model.Favorite
import surabaya.dicoding.com.footballmatchschedule.model.MainMatch
import surabaya.dicoding.com.footballmatchschedule.model.TeamResponse
import surabaya.dicoding.com.footballmatchschedule.tools.Helper
import surabaya.dicoding.com.footballmatchschedule.tools.database
import surabaya.dicoding.com.footballmatchschedule.ui.view.DetailView
import surabaya.dicoding.com.footballmatchschedule.R.menu.detail_menu
import javax.inject.Inject

/**
 * Created by handy on 18/10/18.
 * akang.handy95@gmail.com
 */

class DetailActivity : AppCompatActivity(),AnkoLogger,DetailView.View {

    @Inject
    lateinit var presenter : DetailView.Presenter

    private var menuItem: Menu? = null
    lateinit var eventId : String
    lateinit var homeTeamId : String
    lateinit var awayTeamId : String
    lateinit var dateString: String
    lateinit var homeNameString: String
    lateinit var awayNameString: String
    lateinit var homeScoreString: String
    lateinit var awayScoreString: String
    lateinit var type : String
    var isFavorite: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        injectDepedencyInjection()

        presenter.attach(this)
        presenter.subscribe()
        progressbar.visibility = View.VISIBLE
        eventId = intent.getStringExtra("event_id")
        homeTeamId = intent.getStringExtra("home_team_id")
        awayTeamId = intent.getStringExtra("away_team_id")
        type = intent.getStringExtra("type")

        if (type.equals("favorite")) {
            progressbar.visibility = View.GONE
        }

        favoriteState()
        Log.e("eventId",eventId)
        presenter.loadDataEventDetail(eventId)
        presenter.loadDataTeam(homeTeamId,"home")
        presenter.loadDataTeam(awayTeamId,"away")
        setSupportActionBar(main_toolbar)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = "Loading"
    }

    override fun showDetailEvent(list: MainMatch) {
        if (type.equals("favorite")) {
            progressbar.visibility = View.GONE
        }
        supportActionBar!!.title = "${list.events[0].strHomeTeam} VS ${list.events[0].strAwayTeam}"
        event_date.text = Helper.formatDate(list.events[0].dateEvent)
        home_team.text  = list.events[0].strHomeTeam
        away_team.text  = list.events[0].strAwayTeam
        home_score.text = list.events[0].intHomeScore
        away_score.text = list.events[0].intAwayScore
        home_lineup_goalkeeper.text = list.events[0].strHomeLineupGoalkeeper.replace("; ", "")
        away_lineup_goalkeeper.text = list.events[0].strAwayLineupGoalkeeper.replace("; ", "")
        home_lineup_defense.text = list.events[0].strHomeLineupDefense.replace("; ", "\n")
        away_lineup_defense.text = list.events[0].strAwayLineupDefense.replace("; ", "\n")
        home_lineup_midfield.text = list.events[0].strHomeLineupMidfield.replace("; ", "\n")
        away_lineup_midfield.text = list.events[0].strAwayLineupMidfield.replace("; ", "\n")
        home_lineup_forward.text = list.events[0].strHomeLineupForward.replace("; ", "\n")
        away_lineup_forward.text = list.events[0].strAwayLineupForward.replace("; ", "\n")
        home_lineup_subtitutes.text = list.events[0].strHomeLineupSubstitutes.replace("; ", "\n")
        away_lineup_subtitutes.text = list.events[0].strAwayLineupSubstitutes.replace("; ", "\n")

        dateString = Helper.formatDate(list.events[0].dateEvent)
        homeNameString = list.events[0].strHomeTeam
        awayNameString = list.events[0].strAwayTeam
        homeScoreString = list.events[0].intHomeScore
        awayScoreString = list.events[0].intAwayScore
    }

    override fun showTeamlist(list: TeamResponse, status : String) {
        when(status) {
            "home" -> {
                Glide.with(this).load(list.teams[0].strTeamBadge).into(home_team_badge)
            }
            "away" -> {
                Glide.with(this).load(list.teams[0].strTeamBadge).into(away_team_badge)
            }
        }
    }

    override fun showProgressBar(show: Boolean) {
        if (show) {
            progressbar.visibility = View.VISIBLE
        } else {
            progressbar.visibility = View.GONE
        }
    }

    override fun showErrorMessage(messageError: String) {
        progressbar.visibility = View.VISIBLE
        Log.e("dataLog",messageError)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.unsubscribe()
    }

    private fun favoriteState() {
        database.use {
            val result = select(Favorite.TABLE_FAVORITE)
                    .whereArgs("(HOME_ID = {id}) and (AWAY_ID = {id2})",
                            "id" to homeTeamId,
                            "id2" to awayTeamId)
            val favorite = result.parseList(classParser<Favorite>())
            Log.d("dataLog",favorite.toString())
            if (!favorite.isEmpty()) isFavorite = true
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        if (!type.equals("favorite")) {
            menuInflater.inflate(detail_menu, menu)
            menuItem = menu
            setFavorite()
        }
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
            add_to_favorite -> {
                if (isFavorite) removeFromFavorite()
                else addToFavorite()
                isFavorite = !isFavorite
                setFavorite()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    fun injectDepedencyInjection(){
        val activityComponent = DaggerActivityComponent
                .builder()
                .activityModule(ActivityModule(this))
                .build()
        activityComponent.inject(this)
    }

    private fun addToFavorite() {
        try {
            database.use {
                insert(Favorite.TABLE_FAVORITE,
                        Favorite.HOME_ID to homeTeamId,
                        Favorite.AWAY_ID to awayTeamId,
                        Favorite.EVENT_DATE to eventId,
                        Favorite.EVENT_ID to dateString,
                        Favorite.HOME_NAME to homeNameString,
                        Favorite.AWAY_NAME to awayNameString,
                        Favorite.HOME_SCORE to homeScoreString,
                        Favorite.AWAY_SCORE to awayScoreString)
            }
            Log.d("dateString",dateString)
            toast("Ditambahkan pada favorite")
        } catch (e: SQLiteConstraintException) {
            toast(e.localizedMessage)
        }
    }
    private fun removeFromFavorite() {
        try {
            database.use {
                delete(Favorite.TABLE_FAVORITE,
                        "(HOME_ID = {id}) and (AWAY_ID = {id2}) and (EVENT_ID = {id3})",
                        "id" to homeTeamId,
                        "id2" to awayTeamId,
                        "id3" to eventId)
            }
            toast("Favorite telah di hapus")
        } catch (e: SQLiteConstraintException) {
            toast(e.localizedMessage)
        }
    }

    private fun setFavorite() {
        if (isFavorite)
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, ic_added_to_favorites)
        else
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, ic_add_to_favorites)
    }
}