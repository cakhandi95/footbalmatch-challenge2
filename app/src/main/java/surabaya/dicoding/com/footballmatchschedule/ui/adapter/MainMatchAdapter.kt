package surabaya.dicoding.com.footballmatchschedule.ui.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.sdk25.coroutines.onClick
import surabaya.dicoding.com.footballmatchschedule.R
import surabaya.dicoding.com.footballmatchschedule.model.Event
import surabaya.dicoding.com.footballmatchschedule.model.MainMatch
import surabaya.dicoding.com.footballmatchschedule.tools.Helper
import surabaya.dicoding.com.footballmatchschedule.ui.layout.MainMatchUI

/**
 * Created by handy on 24/09/18.
 * akang.handy95@gmail.com
 */

class MainMatchAdapter(private var mainMatch : List<Event>, private val listener:(Event) -> Unit)
    : RecyclerView.Adapter<MainMatchViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainMatchViewHolder {
        return MainMatchViewHolder(MainMatchUI().createView(AnkoContext.create(parent.context,parent)))
    }

    override fun getItemCount(): Int {
        return mainMatch.size
    }

    override fun onBindViewHolder(holder: MainMatchViewHolder, position: Int) {
        holder.bind(mainMatch[position],listener)
    }
}

class MainMatchViewHolder (itemView: View) : RecyclerView.ViewHolder (itemView) {
    private var homeScore : TextView = itemView.findViewById(R.id.home_score)
    private var awayScore : TextView = itemView.findViewById(R.id.away_score)
    private var teamA : TextView = itemView.findViewById(R.id.home_team)
    private var teamB : TextView = itemView.findViewById(R.id.away_team)
    private var dateScore : TextView = itemView.findViewById(R.id.dateScore)

    fun bind(mainMatch: Event, listener: (Event) -> Unit) {
        teamA.text = mainMatch.strHomeTeam
        teamB.text = mainMatch.strAwayTeam
        homeScore.text = mainMatch.intHomeScore
        awayScore.text = mainMatch.intAwayScore
        dateScore.text = Helper.formatDate(mainMatch.dateEvent)

        itemView.onClick { listener(mainMatch) }
    }
}
