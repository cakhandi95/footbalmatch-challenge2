package surabaya.dicoding.com.footballmatchschedule.tools

import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by handy on 20/10/18.
 * akang.handy95@gmail.com
 */
object Helper {

    val formatterOld: SimpleDateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.US)
    val formatterNew: SimpleDateFormat = SimpleDateFormat("EEE, dd MMM yyyy",Locale.US)

    fun formatDate(input: String?) : String {
        return formatterNew.format(formatterOld.parse(input))
    }
}