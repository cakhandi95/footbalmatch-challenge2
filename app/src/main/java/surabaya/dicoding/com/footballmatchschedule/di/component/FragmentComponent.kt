package surabaya.dicoding.com.footballmatchschedule.di.component

import dagger.Component
import surabaya.dicoding.com.footballmatchschedule.di.module.FragmentModule
import surabaya.dicoding.com.footballmatchschedule.ui.fragment.NextMatchFragment
import surabaya.dicoding.com.footballmatchschedule.ui.fragment.PrevMatchFragment

/**
 * Created by handy on 22/09/18.
 * akang.handy95@gmail.com
 */

@Component(modules = arrayOf(FragmentModule::class))
interface FragmentComponent {

    fun inject(nextMatchFragment: NextMatchFragment)

    fun inject(prevMatchFragment: PrevMatchFragment)
}