package surabaya.dicoding.com.footballmatchschedule.model

import com.google.gson.annotations.SerializedName

data class MainMatch(
        @SerializedName("events") val events: List<Event>
)