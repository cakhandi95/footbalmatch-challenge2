package surabaya.dicoding.com.footballmatchschedule.di.scope

import javax.inject.Qualifier

/**
 * Created by handy on 22/09/18.
 * akang.handy95@gmail.com
 */

@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class PerApplication