package surabaya.dicoding.com.footballmatchschedule.di.module

import android.app.Activity
import dagger.Module
import dagger.Provides
import surabaya.dicoding.com.footballmatchschedule.ui.presenter.DetailPresenter
import surabaya.dicoding.com.footballmatchschedule.ui.presenter.MainPresenter
import surabaya.dicoding.com.footballmatchschedule.ui.view.DetailView
import surabaya.dicoding.com.footballmatchschedule.ui.view.MainView

/**
 * Created by handy on 22/09/18.
 * akang.handy95@gmail.com
 */
@Module
class ActivityModule (private var activity: Activity) {

    @Provides
    fun provideActivity() : Activity {
        return activity
    }

    @Provides
    fun providePresenter(): MainView.Presenter {
        return MainPresenter()
    }

    @Provides
    fun provideDetailEventPresenter(): DetailView.Presenter {
        return DetailPresenter()
    }
}