package surabaya.dicoding.com.footballmatchschedule.di.module

import android.app.Application
import dagger.Module
import dagger.Provides
import surabaya.dicoding.com.footballmatchschedule.BaseApp
import surabaya.dicoding.com.footballmatchschedule.di.scope.PerApplication
import javax.inject.Singleton

/**
 * Created by handy on 22/09/18.
 * akang.handy95@gmail.com
 */
@Module
class ApplicationModule(private val baseApp: BaseApp){

    @Provides
    @Singleton
    @PerApplication
    fun provideApplication(): Application {
        return baseApp
    }
}