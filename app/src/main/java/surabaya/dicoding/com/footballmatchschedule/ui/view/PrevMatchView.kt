package surabaya.dicoding.com.footballmatchschedule.ui.view

import surabaya.dicoding.com.footballmatchschedule.base.BasePresenter
import surabaya.dicoding.com.footballmatchschedule.base.BaseView
import surabaya.dicoding.com.footballmatchschedule.model.LeagueResponse
import surabaya.dicoding.com.footballmatchschedule.model.MainMatch

/**
 * Created by handy on 23/09/18.
 * akang.handy95@gmail.com
 */

class PrevMatchView {

    interface View : BaseView.View {
        fun showDataLeagueList(list: LeagueResponse)
        fun showScoreList(list : MainMatch)
        fun showErrorMessage(messageError: String)
        fun showProgressBar(show : Boolean)
    }

    interface Presenter : BasePresenter.Presenter<View> {
        fun loadDataLeagueList()
        fun loadDataScoreList(id : String)
    }
}