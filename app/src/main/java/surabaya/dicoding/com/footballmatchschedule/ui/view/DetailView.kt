package surabaya.dicoding.com.footballmatchschedule.ui.view

import surabaya.dicoding.com.footballmatchschedule.base.BasePresenter
import surabaya.dicoding.com.footballmatchschedule.base.BaseView
import surabaya.dicoding.com.footballmatchschedule.model.MainMatch
import surabaya.dicoding.com.footballmatchschedule.model.TeamResponse

/**
 * Created by handy on 20/10/18.
 * akang.handy95@gmail.com
 */
class DetailView {

    interface View : BaseView.View {
        fun showDetailEvent (list : MainMatch)
        fun showTeamlist (list: TeamResponse, status : String)
        fun showErrorMessage(messageError: String)
        fun showProgressBar(show : Boolean)
    }

    interface Presenter : BasePresenter.Presenter<View> {
        fun loadDataEventDetail(teamId : String)
        fun loadDataTeam(id: String, status : String)
    }
}