package surabaya.dicoding.com.footballmatchschedule.ui.presenter

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import surabaya.dicoding.com.footballmatchschedule.api.ApiClient
import surabaya.dicoding.com.footballmatchschedule.model.Country
import surabaya.dicoding.com.footballmatchschedule.model.LeagueResponse
import surabaya.dicoding.com.footballmatchschedule.model.MainMatch
import surabaya.dicoding.com.footballmatchschedule.ui.view.NextMathView

/**
 * Created by handy on 23/09/18.
 * akang.handy95@gmail.com
 */

class NextMatchPresenter : NextMathView.Presenter {

    val subscriptions = CompositeDisposable()
    val api: ApiClient = ApiClient.create()
    lateinit var view: NextMathView.View

    override fun subscribe() {

    }

    override fun unsubscribe() {
        subscriptions.clear()
    }

    override fun loadDataLeagueList() {
        var subscribe = api.getLeagues()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({list : LeagueResponse ->
                    view.showProgressBar(false)
                    view.showDataLeagueList(list)
                },{ error ->
                    view.showProgressBar(false)
                    view.showErrorMessage(error.localizedMessage)
                })
        subscriptions.add(subscribe)
    }

    override fun loadDataScoreList(id: String) {
        var subscribe = api.getNextMatch(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({list : MainMatch ->
                    view.showProgressBar(false)
                    view.showScoreList(list)
                },{ error ->
                    view.showProgressBar(false)
                    view.showErrorMessage(error.localizedMessage)
                })
        subscriptions.add(subscribe)
    }

    override fun attach(view: NextMathView.View) {
        this.view = view
    }

    override fun detachView() {
        subscriptions.dispose()
    }
}