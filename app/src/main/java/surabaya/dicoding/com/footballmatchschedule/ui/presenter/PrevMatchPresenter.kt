package surabaya.dicoding.com.footballmatchschedule.ui.presenter

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import surabaya.dicoding.com.footballmatchschedule.api.ApiClient
import surabaya.dicoding.com.footballmatchschedule.model.Country
import surabaya.dicoding.com.footballmatchschedule.model.LeagueResponse
import surabaya.dicoding.com.footballmatchschedule.model.MainMatch
import surabaya.dicoding.com.footballmatchschedule.ui.view.PrevMatchView

/**
 * Created by handy on 23/09/18.
 * akang.handy95@gmail.com
 */

class PrevMatchPresenter : PrevMatchView.Presenter {

    var subscriptions = CompositeDisposable()
    var api: ApiClient = ApiClient.create()
    lateinit var view: PrevMatchView.View

    override fun subscribe() {

    }

    override fun unsubscribe() {
        subscriptions.clear()
    }

    override fun loadDataScoreList(id: String) {
        var loadLeague = api.getPrevMatch(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({list : MainMatch ->
                    view.showProgressBar(false)
                    view.showScoreList(list)
                },{ error->
                    view.showProgressBar(false)
                    view.showErrorMessage(error.localizedMessage)
                })
        subscriptions.add(loadLeague)
    }

    override fun loadDataLeagueList() {
        var loadLeague = api.getLeagues()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({list : LeagueResponse ->
                    view.showProgressBar(false)
                    view.showDataLeagueList(list)
                },{ error->
                    view.showProgressBar(false)
                    view.showErrorMessage(error.localizedMessage)
                })
        subscriptions.add(loadLeague)
    }

    override fun attach(view: PrevMatchView.View) {
        this.view = view
    }

    override fun detachView() {
        subscriptions.dispose()
    }
}