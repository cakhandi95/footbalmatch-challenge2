package surabaya.dicoding.com.footballmatchschedule.api

import io.reactivex.Observable
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.QueryMap
import surabaya.dicoding.com.footballmatchschedule.model.Country
import surabaya.dicoding.com.footballmatchschedule.model.LeagueResponse
import surabaya.dicoding.com.footballmatchschedule.model.MainMatch
import surabaya.dicoding.com.footballmatchschedule.model.TeamResponse

/**
 * Created by handy on 20/09/18.
 * akang.handy95@gmail.com
 */

interface ApiClient {

    @GET("api/v1/json/1/search_all_leagues.php?s=Soccer")
    fun getLeagues() : Observable <LeagueResponse>

    @GET("api/v1/json/1/eventsnextleague.php")
    fun getNextMatch(@Query("id") idNextMatch :String) : Observable<MainMatch>

    @GET("api/v1/json/1/eventspastleague.php")
    fun getPrevMatch(@Query("id") idPrevMatch : String): Observable<MainMatch>

    @GET("api/v1/json/1/lookupteam.php")
    fun getTeam(@Query("id") teamId : String): Observable<TeamResponse>

    @GET("api/v1/json/1/lookupevent.php")
    fun getDetailEvent(@Query("id") teamId : String): Observable<MainMatch>

    companion object ApiServer {
        fun create(): ApiClient {
            val retrofit = retrofit2.Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl("https://www.thesportsdb.com/")
                    .build()
            return retrofit.create(ApiClient::class.java)
        }
    }
}