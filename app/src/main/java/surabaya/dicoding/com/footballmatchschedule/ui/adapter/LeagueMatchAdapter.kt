package surabaya.dicoding.com.footballmatchschedule.ui.adapter

import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.LinearLayout
import org.jetbrains.anko.*
import surabaya.dicoding.com.footballmatchschedule.model.Country
import surabaya.dicoding.com.footballmatchschedule.model.LeagueResponse

/**
 * Created by handy on 23/09/18.
 * akang.handy95@gmail.com
 */

class LeagueMatchAdapter(var data : List<Country>) : BaseAdapter() {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View  {
        val itemLeague = getItem(position)
        return with(parent!!.context) {
            linearLayout {
                lparams{
                    leftPadding = dip(8)
                    rightPadding = dip(8)
                    topPadding = dip(4)
                    bottomPadding = dip(4)
                }
                orientation = LinearLayout.HORIZONTAL

                textView(itemLeague.strLeague) {
                    textSize = 18f
                }
            }
        }

    }

    override fun getItem(position: Int): Country {
        return data.get(position)
    }

    override fun getItemId(position: Int): Long {
        return getItem(position).idLeague.toLong()
    }

    override fun getCount(): Int {
        return data.size
    }
}