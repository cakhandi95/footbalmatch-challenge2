package surabaya.dicoding.com.footballmatchschedule.base

import android.support.v4.app.Fragment

/**
 * Created by handy on 22/09/18.
 * akang.handy95@gmail.com
 */

interface BaseCallBack {

    fun onChangeFragment(fragment: Fragment, enableAnimation: Boolean)
}