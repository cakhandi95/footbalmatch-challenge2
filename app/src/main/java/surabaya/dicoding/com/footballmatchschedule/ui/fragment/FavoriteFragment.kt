package surabaya.dicoding.com.footballmatchschedule.ui.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_favorite.*
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.select
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.onRefresh
import org.jetbrains.anko.support.v4.startActivity
import surabaya.dicoding.com.footballmatchschedule.R
import surabaya.dicoding.com.footballmatchschedule.model.Favorite
import surabaya.dicoding.com.footballmatchschedule.tools.database
import surabaya.dicoding.com.footballmatchschedule.ui.DetailActivity
import surabaya.dicoding.com.footballmatchschedule.ui.adapter.TeamFavoriteAdapter

/**
 * Created by handy on 21/10/18.
 * akang.handy95@gmail.com
 */

class FavoriteFragment : Fragment(){

    private var favorites: MutableList<Favorite> = mutableListOf()
    private lateinit var rootView: View
    private lateinit var favoriteAdapter: TeamFavoriteAdapter

    companion object {
        var TAG = "FavoriteFragment"
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        favoriteAdapter = TeamFavoriteAdapter(favorites) {
            startActivity<DetailActivity>(
                    "event_id" to it.eventId,
                    "home_team_id" to it.HomeId,
                    "away_team_id" to it.AwayId,
                    "type" to "favorite")
        }
        rvlist_favorite.layoutManager = LinearLayoutManager(ctx)
        rvlist_favorite.adapter = favoriteAdapter
        showFavorite()
        swipe_refresh_favorite.onRefresh {
            favorites.clear()
            showFavorite()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        rootView = inflater.inflate(R.layout.fragment_favorite,container,false)
        return rootView
    }

    private fun showFavorite(){
        context?.database?.use {
            swipe_refresh_favorite.isRefreshing = false
            val result = select(Favorite.TABLE_FAVORITE)
            val favorite = result.parseList(classParser<Favorite>())
            Log.d("dataLog2",favorite.toString())
            if (!favorite.isEmpty()) {
                favorites.addAll(favorite)
                favoriteAdapter.notifyDataSetChanged()
            } else {
                rvlist_favorite.visibility = View.GONE
                image_warning.visibility = View.VISIBLE
            }
        }
    }
}