package surabaya.dicoding.com.footballmatchschedule.ui.presenter

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import surabaya.dicoding.com.footballmatchschedule.api.ApiClient
import surabaya.dicoding.com.footballmatchschedule.model.MainMatch
import surabaya.dicoding.com.footballmatchschedule.model.TeamResponse
import surabaya.dicoding.com.footballmatchschedule.ui.view.DetailView

/**
 * Created by handy on 20/10/18.
 * akang.handy95@gmail.com
 */

class DetailPresenter : DetailView.Presenter {

    var subscriptions = CompositeDisposable()
    var api: ApiClient = ApiClient.create()
    lateinit var view: DetailView.View

    override fun subscribe() {

    }

    override fun unsubscribe() {
        subscriptions.clear()
    }

    override fun loadDataEventDetail(teamId: String) {
        val subcribe = api.getDetailEvent(teamId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe ({ list : MainMatch ->
                    view.showDetailEvent(list)
                    view.showProgressBar(false)
                }, {error ->
                    view.showProgressBar(false)
                    view.showErrorMessage(error.localizedMessage)
                })
        subscriptions.add(subcribe)
    }

    override fun loadDataTeam(id: String, status: String) {
        val subcribe = api.getTeam(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe ({ list : TeamResponse ->
                    view.showTeamlist(list,status)
                    view.showProgressBar(false)
                }, {error ->
                    view.showProgressBar(false)
                    view.showErrorMessage(error.localizedMessage)
                })
        subscriptions.add(subcribe)
    }


    override fun attach(view: DetailView.View) {
        this.view = view
    }

    override fun detachView() {
        subscriptions.dispose()
    }
}