package surabaya.dicoding.com.footballmatchschedule

import android.content.Context
import android.support.multidex.MultiDex
import android.support.multidex.MultiDexApplication
import surabaya.dicoding.com.footballmatchschedule.di.component.ApplicationComponent
import surabaya.dicoding.com.footballmatchschedule.di.component.DaggerApplicationComponent
import surabaya.dicoding.com.footballmatchschedule.di.module.ApplicationModule

/**
 * Created by handy on 22/09/18.
 * akang.handy95@gmail.com
 */

class BaseApp : MultiDexApplication() {

    lateinit var component: ApplicationComponent

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(base)
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        component = DaggerApplicationComponent.builder()
                .applicationModule(ApplicationModule(this)).build()
        component.inject(this)
    }

    companion object {
        lateinit var instance: BaseApp private set
    }
}