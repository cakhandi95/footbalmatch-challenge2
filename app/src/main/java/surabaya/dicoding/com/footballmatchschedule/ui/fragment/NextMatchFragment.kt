package surabaya.dicoding.com.footballmatchschedule.ui.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import kotlinx.android.synthetic.main.fragment_match.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.onRefresh
import org.jetbrains.anko.support.v4.startActivity
import surabaya.dicoding.com.footballmatchschedule.R
import surabaya.dicoding.com.footballmatchschedule.di.component.DaggerFragmentComponent
import surabaya.dicoding.com.footballmatchschedule.di.module.FragmentModule
import surabaya.dicoding.com.footballmatchschedule.model.LeagueResponse
import surabaya.dicoding.com.footballmatchschedule.model.MainMatch
import surabaya.dicoding.com.footballmatchschedule.ui.DetailActivity
import surabaya.dicoding.com.footballmatchschedule.ui.adapter.LeagueMatchAdapter
import surabaya.dicoding.com.footballmatchschedule.ui.adapter.MainMatchAdapter
import surabaya.dicoding.com.footballmatchschedule.ui.view.NextMathView
import javax.inject.Inject

/**
 * Created by handy on 23/09/18.
 * akang.handy95@gmail.com
 */

class NextMatchFragment : Fragment(),AnkoLogger,NextMathView.View {

    @Inject
    lateinit var presenter : NextMathView.Presenter
    
    private lateinit var rootView: View
    private lateinit var leagueMatchAdapter: LeagueMatchAdapter
    private lateinit var mainMatchAdapter: MainMatchAdapter
    private lateinit var leagueId : String

    companion object {
        val TAG: String = "NextMatchFragment"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        injectDependencyInjection()
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        rootView = inflater.inflate(R.layout.fragment_match, container, false)
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attach(this)
        presenter.subscribe()
        presenter.loadDataLeagueList()
        progressbar.visibility = View.VISIBLE
        swipe_refresh.onRefresh {
            progressbar.visibility = View.VISIBLE
            presenter.loadDataLeagueList()
        }
    }

    override fun showDataLeagueList(list: LeagueResponse) {
        leagueMatchAdapter = LeagueMatchAdapter(list.countrys)
        spin_leagues.adapter = leagueMatchAdapter

        spin_leagues.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                leagueId = id.toString()
                presenter.loadDataScoreList(leagueId)
                progressbar.visibility = View.VISIBLE
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }
        }
    }

    override fun showScoreList(list: MainMatch) {
        mainMatchAdapter = MainMatchAdapter(list.events) {
            startActivity<DetailActivity>(
                    "event_id" to it.idEvent,
                    "home_team_id" to it.idHomeTeam,
                    "away_team_id" to it.idAwayTeam,
                    "type" to "json"
            )
        }
        rvlist_event.layoutManager = LinearLayoutManager(ctx)
        rvlist_event.adapter = mainMatchAdapter
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.unsubscribe()
    }

    override fun showErrorMessage(messageError: String) {
        Log.e("dataLog", messageError)
    }

    override fun showProgressBar(show: Boolean) {
        if (show) {
            progressbar.visibility = View.VISIBLE
        } else {
            progressbar.visibility = View.GONE
        }
    }

    private fun injectDependencyInjection() {
        val listComponent = DaggerFragmentComponent.builder()
                .fragmentModule(FragmentModule())
                .build()
        listComponent.inject(this)
    }
}