package surabaya.dicoding.com.footballmatchschedule.ui.presenter

import io.reactivex.disposables.CompositeDisposable
import surabaya.dicoding.com.footballmatchschedule.ui.view.MainView

/**
 * Created by handy on 23/09/18.
 * akang.handy95@gmail.com
 */

class MainPresenter : MainView.Presenter {

    val subscriptions = CompositeDisposable()
    lateinit var view: MainView.View

    override fun subscribe() {

    }

    override fun unsubscribe() {
        subscriptions.clear()
    }

    override fun attach(view: MainView.View) {
        this.view = view
        view.showPrevMatchFragment()
        view.showNextMatchFragment()
        view.showFavoriteMatch()
    }

    override fun detachView() {
        subscriptions.dispose()
    }

    override fun onClickAbout() {
        view.showAboutMeFragment()
    }
}