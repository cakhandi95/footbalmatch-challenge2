package surabaya.dicoding.com.footballmatchschedule.base

import android.support.v7.app.AppCompatActivity

/**
 * Created by handy on 20/09/18.
 * akang.handy95@gmail.com
 */

class BaseActivity : AppCompatActivity() {

    override fun onDestroy() {
        super.onDestroy()
    }
}