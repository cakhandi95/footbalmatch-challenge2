package surabaya.dicoding.com.footballmatchschedule.ui.view

import surabaya.dicoding.com.footballmatchschedule.base.BasePresenter
import surabaya.dicoding.com.footballmatchschedule.base.BaseView

/**
 * Created by handy on 23/09/18.
 * akang.handy95@gmail.com
 */

class MainView {

    interface View: BaseView.View {
        fun showPrevMatchFragment()
        fun showNextMatchFragment()
        fun showFavoriteMatch()
        fun showAboutMeFragment()
    }

    interface Presenter: BasePresenter.Presenter<View> {
        fun onClickAbout()
    }
}