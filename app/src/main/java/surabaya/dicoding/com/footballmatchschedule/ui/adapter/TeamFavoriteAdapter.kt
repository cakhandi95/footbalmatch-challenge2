package surabaya.dicoding.com.footballmatchschedule.ui.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.sdk25.coroutines.onClick
import surabaya.dicoding.com.footballmatchschedule.R
import surabaya.dicoding.com.footballmatchschedule.model.Favorite
import surabaya.dicoding.com.footballmatchschedule.tools.Helper
import surabaya.dicoding.com.footballmatchschedule.ui.layout.MainMatchUI

/**
 * Created by handy on 21/10/18.
 * akang.handy95@gmail.com
 */
class TeamFavoriteAdapter(private var favorites : List<Favorite>, private val listener:(Favorite) -> Unit): RecyclerView.Adapter<TeamFavoriteViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TeamFavoriteViewHolder {
        return TeamFavoriteViewHolder(MainMatchUI().createView(AnkoContext.create(parent.context,parent)))
    }

    override fun getItemCount(): Int {
        return favorites.size
    }

    override fun onBindViewHolder(holder: TeamFavoriteViewHolder, position: Int) {
        holder.bind(favorites[position],listener)
    }
}

class TeamFavoriteViewHolder (itemView : View) : RecyclerView.ViewHolder (itemView) {

    private var homeScore : TextView = itemView.findViewById(R.id.home_score)
    private var awayScore : TextView = itemView.findViewById(R.id.away_score)
    private var teamA : TextView = itemView.findViewById(R.id.home_team)
    private var teamB : TextView = itemView.findViewById(R.id.away_team)
    private var dateScore : TextView = itemView.findViewById(R.id.dateScore)

    fun bind(favorite: Favorite, listener: (Favorite) -> Unit) {
        teamA.text = favorite.HomeName
        teamB.text = favorite.AwayName
        homeScore.text = favorite.HomeScore
        awayScore.text = favorite.AwayScore
        dateScore.text = favorite.eventDate

        itemView.onClick { listener(favorite) }
    }
}


