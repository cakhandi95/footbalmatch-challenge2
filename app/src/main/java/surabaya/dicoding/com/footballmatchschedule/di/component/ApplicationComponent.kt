package surabaya.dicoding.com.footballmatchschedule.di.component

import dagger.Component
import surabaya.dicoding.com.footballmatchschedule.BaseApp
import surabaya.dicoding.com.footballmatchschedule.di.module.ApplicationModule

/**
 * Created by handy on 22/09/18.
 * akang.handy95@gmail.com
 */

@Component(modules = arrayOf(ApplicationModule::class))
interface ApplicationComponent {

    fun inject(application: BaseApp)

}