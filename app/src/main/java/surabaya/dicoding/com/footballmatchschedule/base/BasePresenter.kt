package surabaya.dicoding.com.footballmatchschedule.base

/**
 * Created by handy on 22/09/18.
 * akang.handy95@gmail.com
 */

class BasePresenter {

    interface Presenter<in T> {
        fun subscribe()
        fun unsubscribe()
        fun attach(view: T)
        fun detachView()
    }
}