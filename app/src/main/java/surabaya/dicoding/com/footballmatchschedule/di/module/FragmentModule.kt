package surabaya.dicoding.com.footballmatchschedule.di.module

import dagger.Module
import dagger.Provides
import surabaya.dicoding.com.footballmatchschedule.api.ApiClient
import surabaya.dicoding.com.footballmatchschedule.ui.presenter.NextMatchPresenter
import surabaya.dicoding.com.footballmatchschedule.ui.presenter.PrevMatchPresenter
import surabaya.dicoding.com.footballmatchschedule.ui.view.NextMathView
import surabaya.dicoding.com.footballmatchschedule.ui.view.PrevMatchView

/**
 * Created by handy on 22/09/18.
 * akang.handy95@gmail.com
 */
@Module
class FragmentModule {

    @Provides
    fun provideNextMatchPresenter() : NextMathView.Presenter {
        return NextMatchPresenter()
    }

    @Provides
    fun providePrevMatchPresenter() : PrevMatchView.Presenter {
        return PrevMatchPresenter()
    }

    @Provides
    fun provideApiService(): ApiClient {
        return ApiClient.create()
    }
}