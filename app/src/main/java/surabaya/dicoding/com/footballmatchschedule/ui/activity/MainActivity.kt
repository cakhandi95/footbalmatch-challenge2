package surabaya.dicoding.com.footballmatchschedule.ui.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_detail.*
import kotlinx.android.synthetic.main.activity_main.*
import surabaya.dicoding.com.footballmatchschedule.R
import surabaya.dicoding.com.footballmatchschedule.R.id.*
import surabaya.dicoding.com.footballmatchschedule.R.layout.activity_main
import surabaya.dicoding.com.footballmatchschedule.di.component.DaggerActivityComponent
import surabaya.dicoding.com.footballmatchschedule.di.module.ActivityModule
import surabaya.dicoding.com.footballmatchschedule.ui.fragment.FavoriteFragment
import surabaya.dicoding.com.footballmatchschedule.ui.fragment.NextMatchFragment
import surabaya.dicoding.com.footballmatchschedule.ui.fragment.PrevMatchFragment
import surabaya.dicoding.com.footballmatchschedule.ui.view.MainView
import javax.inject.Inject

class MainActivity : AppCompatActivity(),MainView.View {

    @Inject
    lateinit var presenter : MainView.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(activity_main)
        injectDepedencyInjection()
        presenter.attach(this)
        presenter.subscribe()
        bottom_navigation.setOnNavigationItemSelectedListener {
            item -> when (item.itemId) {
            nextMatch -> {
                showNextMatchFragment()
            }
            prevMatch -> {
                showPrevMatchFragment()
            }
            favoriteMatch -> {
                showFavoriteMatch()
            }
        }
            true
        }
        bottom_navigation.selectedItemId = prevMatch
        setSupportActionBar(main_toolbar_default)
    }

    override fun showPrevMatchFragment() {
        if (supportFragmentManager.findFragmentByTag(PrevMatchFragment.TAG) == null) {
            supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.main_container, PrevMatchFragment().newInstance(), PrevMatchFragment.TAG)
                    .commit()
        }
    }

    override fun showNextMatchFragment() {
        if (supportFragmentManager.findFragmentByTag(NextMatchFragment.TAG) == null) {
            supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.main_container, NextMatchFragment(), NextMatchFragment.TAG)
                    .commit()
        }
    }

    override fun showFavoriteMatch() {
        if (supportFragmentManager.findFragmentByTag(FavoriteFragment.TAG) == null) {
            supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.main_container, FavoriteFragment(), FavoriteFragment.TAG)
                    .commit()
        }
    }

    override fun showAboutMeFragment() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    fun injectDepedencyInjection(){
        val activityComponent = DaggerActivityComponent
                .builder()
                .activityModule(ActivityModule(this))
                .build()
        activityComponent.inject(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.unsubscribe()
    }
}
