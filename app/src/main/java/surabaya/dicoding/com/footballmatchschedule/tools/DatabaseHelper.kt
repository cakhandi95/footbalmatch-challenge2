package surabaya.dicoding.com.footballmatchschedule.tools

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import org.jetbrains.anko.db.*
import surabaya.dicoding.com.footballmatchschedule.model.Favorite

/**
 * Created by handy on 21/10/18.
 * akang.handy95@gmail.com
 */
class DatabaseHelper(ctx : Context) : ManagedSQLiteOpenHelper(ctx,"favorite.db",null,1) {

    companion object {
        private var instance: DatabaseHelper? = null

        @Synchronized
        fun getInstance(ctx: Context): DatabaseHelper {
            if (instance == null) {
                instance = DatabaseHelper(ctx.applicationContext)
            }
            return instance as DatabaseHelper
        }
    }

    override fun onCreate(db: SQLiteDatabase?) {
        db!!.createTable(Favorite.TABLE_FAVORITE, true,
                Favorite.ID to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
                Favorite.HOME_ID to TEXT,
                Favorite.AWAY_ID to TEXT,
                Favorite.EVENT_DATE to TEXT,
                Favorite.EVENT_ID to TEXT,
                Favorite.HOME_NAME to TEXT,
                Favorite.AWAY_NAME to TEXT,
                Favorite.HOME_SCORE to TEXT,
                Favorite.AWAY_SCORE to TEXT)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db!!.dropTable(Favorite.TABLE_FAVORITE, true)
    }
}
val Context.database: DatabaseHelper
    get() = DatabaseHelper.getInstance(applicationContext)