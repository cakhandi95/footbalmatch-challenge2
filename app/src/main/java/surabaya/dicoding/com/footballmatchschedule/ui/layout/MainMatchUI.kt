package surabaya.dicoding.com.footballmatchschedule.ui.layout

import android.graphics.Color
import android.graphics.Typeface
import android.view.Gravity
import android.view.ViewGroup
import android.widget.LinearLayout
import org.jetbrains.anko.*
import surabaya.dicoding.com.footballmatchschedule.R
import surabaya.dicoding.com.footballmatchschedule.R.id.*

/**
 * Created by handy on 25/09/18.
 * akang.handy95@gmail.com
 */

class MainMatchUI : AnkoComponent<ViewGroup> {

    override fun createView(ui: AnkoContext<ViewGroup>) = with(ui) {
        relativeLayout {
            backgroundColor = Color.WHITE
            linearLayout {
                orientation = LinearLayout.VERTICAL
                padding = dip(8)
                textView {
                    id = dateScore
                    text = resources.getString(R.string.event_date)
                    setTypeface(typeface, Typeface.BOLD_ITALIC)
                }.lparams {
                    bottomMargin = dip(8)
                    gravity = Gravity.CENTER_HORIZONTAL
                }
                linearLayout {
                    orientation = LinearLayout.HORIZONTAL
                    textView {
                        id = home_team
                        gravity = Gravity.RIGHT
                        text = resources.getString(R.string.home_team)
                        setTypeface(typeface, Typeface.BOLD)
                    }.lparams(width = 0) {
                        weight = 1f
                    }
                    linearLayout {
                        orientation = LinearLayout.HORIZONTAL
                        gravity = Gravity.CENTER
                        textView {
                            id = home_score
                            text = resources.getString(R.string.x)
                            setTypeface(typeface, Typeface.BOLD)
                        }.lparams {
                            rightMargin = dip(4)
                            marginEnd = dip(4)
                        }
                        textView {
                            text = resources.getString(R.string.vs)
                        }
                        textView {
                            id = away_score
                            text = resources.getString(R.string.y)
                            setTypeface(typeface, Typeface.BOLD)
                        }.lparams {
                            leftMargin = dip(4)
                            marginStart = dip(4)
                        }
                    }.lparams(width = 0) {
                        weight = 1f
                    }
                    textView {
                        id = away_team
                        gravity = Gravity.LEFT
                        text = resources.getString(R.string.away_team)
                        setTypeface(typeface, Typeface.BOLD)
                    }.lparams(width = 0) {
                        weight = 1f
                    }
                }.lparams(width = matchParent)
            }.lparams(width = matchParent)
        }
    }
}